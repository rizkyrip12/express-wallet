const mongoose = require('mongoose');

const customerSchema = mongoose.Schema({
  name: String,
  phoneNumber: String,
  address: String,
  createdAt: Date,
  updatedAt: Date,
});

const Customer = mongoose.model('Customer', customerSchema);

module.exports = Customer;
