const customerRepository = require('../repositories/CustomerRepository');

class CustomerService {
  static getCustomerById = async (id) => {
    return customerRepository.getCustomerById(id);
  }

  static createCustomer = async (payload) => {
    // console.log('payload ', payload);
    return customerRepository.createCustomer(payload);
  }
}

module.exports = CustomerService;
