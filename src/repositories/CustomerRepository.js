const Customer = require('../models/Customer');

class CustomerRepository {
  static getCustomerById = async (id) => {
    const param = {
      _id: id
    };
    return Customer.findById(param);
  }

  static createCustomer = async (payload) => {
    const { name, phoneNumber, address } = payload;
    const newCustomer = new Customer({
      name,
      phoneNumber,
      address,
      createdAt: Date.now(),
      updatedAt: Date.now()
    });
    console.log('new Customer ', newCustomer);
    return newCustomer.save(newCustomer);
  }
}

module.exports = CustomerRepository;
