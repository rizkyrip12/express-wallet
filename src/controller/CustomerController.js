const customerService = require('../services/CustomerService');

const {
  getCustomerById,
  createCustomer
} = customerService;

class CustomerController {
    static getCustomerById = async (req, res) => {
      const { customerId } = req.params;
      try {
        const customer = await getCustomerById(customerId);
        res.json({
          data: customer
        });
      } catch (error) {
        console.log(error);
      }
    }

    static add = async (req, res) => {
      try {
        const savedCourse = await createCustomer(req.body);
        res.json(savedCourse);
      } catch (error) {
        console.log(error);
      }
    };
}

module.exports = CustomerController;
