

const express = require('express');
const customerController = require('../../controller/CustomerController');

const router = express.Router();

router.get('/customers/:customerId', customerController.getCustomerById);
router.post('/customers', customerController.add);

module.exports = router;
