const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const mongoose = require('mongoose');
const routes = require('./src/routes');

const dbURL = require('./properties').DB_URL;


mongoose.connect(dbURL);
mongoose.connection.on('Connected', () => {
  console.log('Connected to MongoDB using MongooseJS');
});
console.log('1', dbURL);

app.use(bodyParser.json());
app.use('/', routes);

app.listen(3019);
